package be.kdg.programming3.BankGilles.programming3;

import be.kdg.programming3.BankGilles.programming3.presentation.controller.BankController;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BanksystemwithspringApplication {

    public static void main(String[] args) {
        SpringApplication.run(BanksystemwithspringApplication.class, args).getBean(BankController.class);
    }

}
