package be.kdg.programming3.BankGilles.programming3.domain;

import be.kdg.programming3.BankGilles.programming3.Banks;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


@Component
@Scope(value = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.TARGET_CLASS)
@Entity(name = "Bank")
@Table(name="Banks")
public class Bank {

    @Column(name = "name", nullable = false, length = 45)
    @Enumerated(EnumType. STRING)
    Banks name;

    @Column(name = "address", nullable = false, length = 45)
    String address;

    @Id
    @GeneratedValue(strategy = GenerationType. IDENTITY)
    @Column(name = "bankId")
    int bankId;


    @Column(name = "country", nullable = false, length = 45)
    String country;



    @OneToMany(mappedBy = "bank", cascade = CascadeType. ALL)
    public List<BankCard> cardList = new ArrayList<>();


    @ManyToMany(cascade = {CascadeType. DETACH, CascadeType.MERGE,
            CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinTable(name="USERS_BANKS",
            joinColumns = @JoinColumn(name = "bankId",referencedColumnName = "BANKID"),
            inverseJoinColumns = @JoinColumn(name="userId",referencedColumnName = "USERID"))
    private List<User> users = new ArrayList<>();




    public Bank(){

    }
    public Bank(Banks name, String address, String country, int bankId) {
        this.name = name;
        this.address = address;
        this.country = country;
        this.bankId = bankId;

    }

    public Bank(Banks name, String address, String country) {
        this.name = name;
        this.address = address;
        this.country = country;
    }


    public void AddUser(User user) {
        users.add(user);
    }

    public void DeleteUser(int id) {
        users.remove(id);
    }


    public void addBankCard(BankCard bankCard) {
        cardList.add(bankCard);
    }

    public List<BankCard> getCardList() {
        return cardList;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setName(Banks name) {
        this.name = name;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getBankId() {
        return bankId;
    }

    public void setBankId(int bankId) {
        this.bankId = bankId;
    }

    public void setCountry(String country) {
        this.country = country;
    }

//    public List<BankCard> getCardList() {
//        return cardList;
//    }
//
//    public void addCard(BankCard card) {
//        cardList.add(card);
//    }

    public Banks getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public String getCountry() {
        return country;
    }


    @Override
    public String toString() {
        return "Bank{" +
                "name=" + name +
                ", address='" + address + '\'' +
                ", country='" + country + '\'' +
                '}';
    }
}
