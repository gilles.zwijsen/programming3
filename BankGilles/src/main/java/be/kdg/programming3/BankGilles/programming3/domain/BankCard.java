package be.kdg.programming3.BankGilles.programming3.domain;

import be.kdg.programming3.BankGilles.programming3.Banks;

import javax.persistence.*;
import java.time.LocalDate;

@Entity(name = "BankCard")
@Table(name="bankcars")
public class BankCard {


    @Column(name = "name", nullable = false, length = 45)
    private String name;




    @Column(name = "IBANNUMBER", nullable = false, length = 50)
    private String IbanNumber;



    @Column(name = "EXPIREDATE", nullable = false, length = 50)
    LocalDate expireDate;

    //@DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate expireDate;

    @Column(name = "amount")
    private double amount;





    @Id
    @GeneratedValue(strategy = GenerationType. IDENTITY)
    @Column(name = "BANKCARDID")
    int id;




    @Column(name = "BANKID",nullable = false)
    private int bankId;



    @Column(name = "USERID", nullable = false)
    private int userId;


    @ManyToOne(cascade = {CascadeType. DETACH,
            CascadeType.MERGE, CascadeType.PERSIST,
            CascadeType.REFRESH})
    @JoinColumn(name="BANKID",insertable = false, updatable = false)
    private Bank bank;


    @ManyToOne(cascade = {CascadeType. DETACH,
            CascadeType.MERGE, CascadeType.PERSIST,
            CascadeType.REFRESH})
    @JoinColumn(name="USERID",insertable = false, updatable = false)
    private User user;





    public BankCard(String name, String ibanNumber, LocalDate expireDate, double amount, Banks bankName) {
        this.name = name;
        IbanNumber = ibanNumber;
        this.expireDate = expireDate;
        this.amount = amount;

    }

    public BankCard(String name, String ibanNumber, LocalDate expireDate, double amount, int bankId, int userId) {
        this.name = name;
        IbanNumber = ibanNumber;
        this.expireDate = expireDate;
        this.amount = amount;

        this.bankId =bankId;
        this.userId = userId;
    }



    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getBankId() {
        return bankId;
    }

    public void setBankId(int bankId) {
        this.bankId = bankId;
    }

    public BankCard(String name, String ibanNumber, LocalDate expireDate, double amount, int id) {
        this.name = name;
        IbanNumber = ibanNumber;
        this.expireDate = expireDate;
        this.amount = amount;

        this.id = id;
    }

    public BankCard(String name, String ibanNumber, LocalDate expireDate, double amount, int id, int BankID,int UserId) {
        this.name = name;
        IbanNumber = ibanNumber;
        this.expireDate = expireDate;
        this.amount = amount;
        this.id = id;
        this.bankId = BankID;
        this.userId = UserId;
    }

    public BankCard() {

    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setIbanNumber(String ibanNumber) {
        IbanNumber = ibanNumber;
    }

    public void setExpireDate(LocalDate expireDate) {
        this.expireDate = expireDate;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }



    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getIbanNumber() {
        return IbanNumber;
    }

    public LocalDate getExpireDate() {
        return expireDate;
    }

    public double getAmount() {
        return amount;
    }



    @Override
    public String toString() {


        return "BankCard: " + name + "\n" +
                "IbanNumber='" + IbanNumber  + "\n" +
                "expireDate=" + expireDate + "\n" +
                "amount=" + amount + "\n"
                ;
    }
}
