package be.kdg.programming3.BankGilles.programming3.domain;

import javax.persistence.*;
import javax.servlet.http.HttpSession;
import java.time.LocalTime;


@Entity(name = "HisorySession")
@Table(name = "HistorySession")
public class HistorySession {

    @Id
    @GeneratedValue(strategy = GenerationType. IDENTITY)
    int historyId;

    String siteVisited;

    LocalTime time;

    public HistorySession(String siteVisited) {
        this.siteVisited = siteVisited;
        this.time = LocalTime.now();
    }

    public HistorySession() {

    }

    public String getSiteVisited() {
        return siteVisited;
    }

    public void setSiteVisited(String siteVisited) {
        this.siteVisited = siteVisited;
    }

    public LocalTime getTime() {
        return time;
    }

    public void setTime(LocalTime time) {
        this.time = time;
    }
}
