package be.kdg.programming3.BankGilles.programming3.domain;


import be.kdg.programming3.BankGilles.programming3.domain.Bank;
import be.kdg.programming3.BankGilles.programming3.domain.BankCard;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity(name = "User")
@Table(name = "Users")
public class User {


    @Id
    @Column(name = "userId")
    @GeneratedValue(strategy = GenerationType. IDENTITY)
    int id;

    @Column(name = "name", nullable = false, length = 50)
    String name;
    @Column(name = "DATEOFBIRTH", nullable = false)
    LocalDate dateOfBirth;

    @Column(name = "address", nullable = false, length = 50)
    String address;

    public User(int id, String name, LocalDate dateOfBirth, String address) {
        this.id = id;
        this.name = name;
        this.dateOfBirth = dateOfBirth;
        this.address = address;
    }
    public User( String name, LocalDate dateOfBirth, String address) {
        this.name = name;
        this.dateOfBirth = dateOfBirth;
        this.address = address;
    }


    @OneToMany(mappedBy = "user", cascade = CascadeType. ALL)
    private transient List<BankCard> bankcards = new ArrayList<>();

    @ManyToMany(cascade = {CascadeType. DETACH, CascadeType.MERGE,
            CascadeType.PERSIST, CascadeType.REFRESH})
    private transient List<Bank> banks = new ArrayList<>();

    public User() {
    }



    public int getId() {
        return id;
    }

    public List<BankCard> getBankcards() {
        return bankcards;
    }

    public void setBankcards(List<BankCard> bankcards) {
        this.bankcards = bankcards;
    }

    public void setBanks(List<Bank> banks) {
        this.banks = banks;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Bank> getBanks() {
        return banks;
    }

    public void addBank(Bank bank) {
        banks.add(bank);
    }

    public void addBankCard(BankCard bankCard) {
        bankcards.add(bankCard);
    }

    public String getName() {
        return name;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public String getAddress() {
        return address;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", dateOfBirth=" + dateOfBirth +
                ", address='" + address + '\'' + banks +
                '}';
    }
}

