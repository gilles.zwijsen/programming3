package be.kdg.programming3.BankGilles.programming3.presentation.ViewModels;

import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.*;
import java.time.LocalDate;

public class bankCardView {

    @NotBlank(message = "name is mandatory")
    @Size(min=5, max=50, message = "Name should have length between 5 and 100")
    private String name;


    @Pattern(regexp = "[a-zA-Z]{2}[0-9]{16}", message = "must have 2 letters than 16 numbers")
    @NotNull(message = "IbanNumber is mandatory")
    @Size(min=18, max=18, message = "Name should have length of 18")
    private String IbanNumber;

    @NotNull(message = "ExpireDate is mandatory")
    @Future(message = "your ExpireDate NOT be in the past")
    @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate expireDate;

    private double amount;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIbanNumber() {
        return IbanNumber;
    }

    public void setIbanNumber(String ibanNumber) {
        IbanNumber = ibanNumber;
    }

    public LocalDate getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(LocalDate expireDate) {
        this.expireDate = expireDate;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }


}
