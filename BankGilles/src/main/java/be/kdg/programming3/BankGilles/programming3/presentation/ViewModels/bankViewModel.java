package be.kdg.programming3.BankGilles.programming3.presentation.ViewModels;

import be.kdg.programming3.BankGilles.programming3.Banks;

import javax.validation.constraints.*;

public class bankViewModel {



    String name;

    @NotBlank(message = "Address is mandatory")
    @Size(min=2, max=50, message = "Address should have length between 5 and 100")
    String address;


    @NotBlank(message = "Country is mandatory")
    @Size(min=5, max=50, message = "Country should have length between 5 and 100")
    String country;


    public Banks getName() {
        return Banks.valueOf(name);
    }

    public String getAddress() {
        return address;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCountry() {
        return country;
    }
}
