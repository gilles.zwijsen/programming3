package be.kdg.programming3.BankGilles.programming3.presentation.ViewModels;

import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import java.time.LocalDate;

public class userViewModel {


    int id;


    @NotBlank(message = "name is mandatory")
    String name;

    @NotNull(message = "ExpireDate is mandatory")
    @Past(message = "your ExpireDate NOT be in the past")
    @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate dateOfBirth;


    @NotBlank(message = "name is mandatory")
    String address;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
