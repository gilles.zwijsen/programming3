package be.kdg.programming3.BankGilles.programming3.presentation.controller;

import be.kdg.programming3.BankGilles.programming3.exceptions.BankCardLimitExecption;
import be.kdg.programming3.BankGilles.programming3.presentation.ViewModels.bankCardView;
import be.kdg.programming3.BankGilles.programming3.service.BankCardService;
import be.kdg.programming3.BankGilles.programming3.service.HistorySessionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
//@RequestMapping("/addBankCard")
public class AddBankCardController {


    private final BankCardService bankCardService;
    private Logger logger = LoggerFactory.getLogger(AddBankController.class);

    private final HistorySessionService historySessionService;

    public AddBankCardController(BankCardService bankCardService,HistorySessionService historySessionService) {
        this.bankCardService = bankCardService;
        this.historySessionService = historySessionService;
    }

    @GetMapping("/addBankCard")
    public String showForm(Model model, HttpSession session) {
        model.addAttribute("formDataa", new bankCardView());
        session.setAttribute("name", "AddBankCard");
        historySessionService.addSession(session);
        logger.info("adding bankcard");
        return "addBankCard";
    }


    @ExceptionHandler(BankCardLimitExecption.class)
    public String handleInsufficientMoneyException(Exception e, Model model){
        logger.error(e.getMessage());
        model.addAttribute("status",e.getMessage());
        model.addAttribute("error", "Not enough money...");
        return "error";
    }



    @RequestMapping(path = "/addBankCard/add", method = RequestMethod.GET)
    public String getPage(@RequestParam(value = "id",required = false) int id, Model model, HttpSession session) {
        model.addAttribute("formDataa", new bankCardView());
        session.setAttribute("name", "AddBankCard");
        session.setAttribute("bankId",id);
        historySessionService.addSession(session);
        return "addBankCard";
    }




    @PostMapping("/addBankCard")
    public String submitForm(@Valid @ModelAttribute("formDataa") bankCardView formDataa,
                             BindingResult errors, HttpSession session, HttpServletRequest request) {

        if (errors.hasErrors()) {
            errors.getAllErrors().forEach(error -> {
                logger.error(error.toString());
            });

            return "addBankCard";


        }

        StringBuilder card = new StringBuilder();
        for (int i = 0; i < formDataa.getIbanNumber().length(); i++) {
            card.append(formDataa.getIbanNumber().charAt(i));

            if (i == 1 || i == 5 || i == 9 || i == 13) {
                card.append("-");
            }


        }

        bankCardService.addBankCard(formDataa.getName(), card.toString(), formDataa.getExpireDate(),
                formDataa.getAmount(),
                (Integer) session.getAttribute("bankId"), (Integer) session.getAttribute("userId"));

        logger.info("added bankCard successfully");
        return "redirect:userItem?id="+ session.getAttribute("userId");
    }


}
