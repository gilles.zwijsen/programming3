package be.kdg.programming3.BankGilles.programming3.presentation.controller;

import be.kdg.programming3.BankGilles.programming3.presentation.ViewModels.bankViewModel;
import be.kdg.programming3.BankGilles.programming3.service.BankService;
import be.kdg.programming3.BankGilles.programming3.service.HistorySessionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
public class AddBankController {
    private Logger logger = LoggerFactory.getLogger(AddBankController.class);
    private final BankService bankService;
    private final HistorySessionService historySessionService;


    public AddBankController(BankService bankService, HistorySessionService historySessionService) {
        this.bankService = bankService;
        this.historySessionService = historySessionService;
    }

    @GetMapping("/addBank")
    public String showForm(Model model, HttpSession session) {
        model.addAttribute("formData", new bankViewModel());
        session.setAttribute("name", "AddBank");
        historySessionService.addSession(session);
        return "addBank";
    }



    @PostMapping("/addBankMethod")
    public String submitForm(@Valid @ModelAttribute("formData") bankViewModel formData, BindingResult errors) {

        if (errors.hasErrors()) {
            errors.getAllErrors().forEach(error -> {
                logger.error(error.toString());
            });

            return "addBank";


        }
        bankService.addBank(formData.getName(),formData.getAddress(),formData.getCountry());
        logger.info("added bank successfully");
        return "redirect:/User";
    }




}
