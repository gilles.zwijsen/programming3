package be.kdg.programming3.BankGilles.programming3.presentation.controller;

import be.kdg.programming3.BankGilles.programming3.presentation.ViewModels.userViewModel;
import be.kdg.programming3.BankGilles.programming3.service.HistorySessionService;
import be.kdg.programming3.BankGilles.programming3.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
public class AddUserController {

    private Logger logger = LoggerFactory.getLogger(AddUserController.class);
    private final  UserService userService;
    private final HistorySessionService historySessionService;


    public AddUserController(UserService userService, HistorySessionService historySessionService) {
        this.userService = userService;
        this.historySessionService = historySessionService;
    }


    @GetMapping("/addUser")
    public String showForm(Model model, HttpSession session) {
        model.addAttribute("formData", new userViewModel());
        session.setAttribute("name", "AddUser");
        historySessionService.addSession(session);
        return "addUser";
    }


    @PostMapping("/addingUser")
    public String submitForm(@Valid @ModelAttribute("formData") userViewModel formData, BindingResult errors) {

        if (errors.hasErrors()) {
            errors.getAllErrors().forEach(error -> {
                logger.error(error.toString());
            });
            return "addUser";
        }
        userService.createUser(formData.getName(),formData.getDateOfBirth(),formData.getAddress());
        logger.info("added user successfully");
        return "redirect:/User";
    }

}
