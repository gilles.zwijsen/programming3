package be.kdg.programming3.BankGilles.programming3.presentation.controller;

import be.kdg.programming3.BankGilles.programming3.service.BankCardService;
import be.kdg.programming3.BankGilles.programming3.service.HistorySessionService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/bankcards")
public class BankCardController {

    private final BankCardService bankService;
    private final HistorySessionService historySessionService;


    public BankCardController(BankCardService bankService, HistorySessionService historySessionService) {
        this.bankService = bankService;
        this.historySessionService = historySessionService;
    }

    @GetMapping
    public ModelAndView showBanks(HttpSession session){
        ModelAndView modelAndView = new ModelAndView("bankcards");
        modelAndView.addObject("bankcards", bankService.getBankCards());
        session.setAttribute("name", "AddBankCard");
        historySessionService.addSession(session);
        return modelAndView;

    }
}
