package be.kdg.programming3.BankGilles.programming3.presentation.controller;

import be.kdg.programming3.BankGilles.programming3.service.BankCardService;
import be.kdg.programming3.BankGilles.programming3.service.HistorySessionService;
import be.kdg.programming3.BankGilles.programming3.presentation.data.JSonWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
public class BankCardItemController {
    private final BankCardService bankCardService;
    private final HistorySessionService historySessionService;
    private Logger logger = LoggerFactory.getLogger(BankCardItemController.class);



    public BankCardItemController(BankCardService bankCardService, HistorySessionService historySessionService) {
        this.bankCardService = bankCardService;
        this.historySessionService = historySessionService;
    }




    @GetMapping("/bankCardItem")
    public ModelAndView showBanks(@RequestParam(value = "id",required = false) int id, HttpSession session){
        ModelAndView modelAndView = new ModelAndView("bankCardItem");
        modelAndView.addObject("bankcards", bankCardService.getBankCardById(id));

        try {
            bankCardService.getBankCardById(id).getName();
        }
        catch (NullPointerException e) {
            logger.error("error retrieving bankcard" + e.getMessage());
            modelAndView = new ModelAndView("error");
            modelAndView.addObject("status","No Bankcard Found");
        }
        session.setAttribute("name", "BankCardItem");
        historySessionService.addSession(session);
        return modelAndView;

    }



    @RequestMapping(path = "/json", method = RequestMethod.GET)
    public String json (Model model, HttpServletRequest request, HttpSession session, RedirectAttributes redirectAttributes) {

        redirectAttributes.addFlashAttribute("message", "Successfully saved as a json file in bankcards.json");
        redirectAttributes.addFlashAttribute("alertClass", "alert-success");
        JSonWriter jSonWriter = new JSonWriter();
        jSonWriter.writeBankCards(bankCardService.getBankCards());
        logger.info("Copied data to jsonfile");
        String referer = request.getHeader("Referer");
        return "redirect:"+ referer;
    }

    @RequestMapping(path = "/bankCardItems/{id}", method = RequestMethod.GET)
    public String loadBankItem (@RequestParam(value = "id",required = false) int id, Model model) {
        model.addAttribute("id",id);
        return "redirect:bankCardItem/" + id;
    }


}
