
package be.kdg.programming3.BankGilles.programming3.presentation.controller;

        import be.kdg.programming3.BankGilles.programming3.service.BankCardService;
        import be.kdg.programming3.BankGilles.programming3.service.HistorySessionService;
        import be.kdg.programming3.BankGilles.programming3.service.UserService;
        import org.springframework.stereotype.Controller;
        import org.springframework.ui.Model;
        import org.springframework.web.bind.annotation.GetMapping;
        import org.springframework.web.bind.annotation.RequestMapping;
        import org.springframework.web.bind.annotation.RequestMethod;
        import org.springframework.web.bind.annotation.RequestParam;
        import org.springframework.web.servlet.ModelAndView;

        import javax.servlet.http.HttpSession;

@Controller
public class BankCardUserController {
    private final BankCardService bankCardService;
    private final HistorySessionService historySessionService;


    public BankCardUserController(BankCardService bankCardService, HistorySessionService historySessionService) {
        this.bankCardService = bankCardService;
        this.historySessionService = historySessionService;
    }

    @GetMapping("/bankCardUser")
    public ModelAndView showBanks(@RequestParam(value = "id",required = false) int id, HttpSession session){
        ModelAndView modelAndView = new ModelAndView("bankCardUser");
        session.setAttribute("bankId",id);
        modelAndView.addObject("bankcards", bankCardService.findByBank(id, (Integer) session.getAttribute("userId")));
        session.setAttribute("name", "BankCardItem");
        historySessionService.addSession(session);
        return modelAndView;

    }

    @RequestMapping(path = "/bankCardUser/{id}", method = RequestMethod.GET)
    public String loadBankItem (@RequestParam(value = "id",required = false) int id, Model model) {
        model.addAttribute("id",id);
        return "redirect:bankCardItem/" + id;
    }


}
