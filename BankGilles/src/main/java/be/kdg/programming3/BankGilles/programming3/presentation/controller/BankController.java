package be.kdg.programming3.BankGilles.programming3.presentation.controller;

import be.kdg.programming3.BankGilles.programming3.service.BankService;
import be.kdg.programming3.BankGilles.programming3.service.HistorySessionService;
import be.kdg.programming3.BankGilles.programming3.service.UserService;
import be.kdg.programming3.BankGilles.programming3.presentation.data.JSonWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/banks")
public class BankController {

    private final BankService bankService;
    private Logger logger = LoggerFactory.getLogger(BankController.class);

    private final HistorySessionService historySessionService;
    private final UserService userService;



    public BankController(BankService bankService, HistorySessionService historySessionService, UserService userService) {
        this.bankService = bankService;
        this.historySessionService = historySessionService;
        this.userService = userService;
    }



    @GetMapping
    public ModelAndView showBanks(HttpSession session){
        ModelAndView modelAndView = new ModelAndView("banks");
        modelAndView.addObject("banks", bankService.getBanks());
        session.setAttribute("name", "AddBankCard");
        historySessionService.addSession(session);
        return modelAndView;

    }


    @RequestMapping(path = "/delete", method = RequestMethod.GET)
    public ModelAndView delete(@RequestParam(value = "id",required = false) int id,HttpSession session){
        ModelAndView modelAndView = new ModelAndView("banks");
        logger.info("Deleting banknumber " + id);
        bankService.deleteBank(id,(Integer) session.getAttribute("userId"));
        modelAndView.addObject("banks",userService.findBankByUser((Integer) session.getAttribute("userId")));
        session.setAttribute("name", "AddBankCard");
        modelAndView.addObject("bankslist",bankService.getBanks());
        historySessionService.addSession(session);
        return modelAndView;

    }


    @RequestMapping(path = "/json", method = RequestMethod.GET)
    public String json (HttpServletRequest request, HttpSession session, RedirectAttributes redirectAttributes) {

        redirectAttributes.addFlashAttribute("message", "Successfully saved as a json file in banks.json");
        redirectAttributes.addFlashAttribute("alertClass", "alert-success");
        JSonWriter jSonWriter = new JSonWriter();
        jSonWriter.writeBanks(userService.findBankByUser((Integer) session.getAttribute("userId")));
        String referer = request.getHeader("Referer");
        logger.info("Copied data to jsonfile");
        return "redirect:"+ referer;
    }


    @RequestMapping(path = "/bankItem", method = RequestMethod.GET)
    public String loadBankItem (@RequestParam(value = "id",required = false) int id, Model model) {
        model.addAttribute("id",id);
        return "redirect:home/" + id;
    }

}
