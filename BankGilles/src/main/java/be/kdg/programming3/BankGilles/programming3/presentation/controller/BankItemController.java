package be.kdg.programming3.BankGilles.programming3.presentation.controller;

import be.kdg.programming3.BankGilles.programming3.service.BankService;
import be.kdg.programming3.BankGilles.programming3.service.HistorySessionService;
import be.kdg.programming3.BankGilles.programming3.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;

@Controller
public class BankItemController {

    private final BankService bankService;
    private final HistorySessionService historySessionService;

    public BankItemController(BankService bankService, HistorySessionService historySessionService) {
        this.bankService = bankService;
        this.historySessionService = historySessionService;
    }



    @GetMapping("/bankItem")
    public ModelAndView showBanks(@RequestParam(value = "id",required = false) int id, HttpSession session){
        ModelAndView modelAndView = new ModelAndView("bankItem");
        modelAndView.addObject("banks", bankService.getBanks().get(id-1));
        session.setAttribute("name", "BankItem");
        historySessionService.addSession(session);
        return modelAndView;

    }

    @RequestMapping(path = "/bankItems/{id}", method = RequestMethod.GET)
    public String loadBankItem (@RequestParam(value = "id",required = false) int id, Model model,HttpSession session) {
        model.addAttribute("id",id);
        return "redirect:bankItem/" + id;
    }





}
