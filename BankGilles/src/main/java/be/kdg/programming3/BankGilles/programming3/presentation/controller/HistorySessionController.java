package be.kdg.programming3.BankGilles.programming3.presentation.controller;


import be.kdg.programming3.BankGilles.programming3.service.HistorySessionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/HistorySession")
public class HistorySessionController {

    private final HistorySessionService historySessionService;


    public HistorySessionController(HistorySessionService historySessionService) {
        this.historySessionService = historySessionService;
    }


    @GetMapping
    public ModelAndView showSessions(HttpSession session){
        ModelAndView modelAndView = new ModelAndView("HistorySession");
        modelAndView.addObject("sessions", historySessionService.getSessions());
        session.setAttribute("name", "HistorySession");
        historySessionService.addSession(session);
        return modelAndView;

    }
}
