package be.kdg.programming3.BankGilles.programming3.presentation.controller;


import be.kdg.programming3.BankGilles.programming3.service.HistorySessionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/home")
public class HomeController {

    private final HistorySessionService historySessionService;


    public HomeController(HistorySessionService historySessionService) {
        this.historySessionService = historySessionService;
    }

    @GetMapping
    public ModelAndView showBanks(HttpSession session){
        ModelAndView modelAndView = new ModelAndView("home");
        modelAndView.addObject("home");
        session.setAttribute("name", "Home");
        historySessionService.addSession(session);
        return modelAndView;

    }
}
