package be.kdg.programming3.BankGilles.programming3.presentation.controller;


import be.kdg.programming3.BankGilles.programming3.service.HistorySessionService;
import be.kdg.programming3.BankGilles.programming3.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;


import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/User")
public class UserController {

    private final UserService userService;
    private Logger logger = LoggerFactory.getLogger(BankController.class);

    private final HistorySessionService historySessionService;


    public UserController(UserService bankService, HistorySessionService historySessionService) {
        this.userService = bankService;
        this.historySessionService = historySessionService;
    }




    @GetMapping
    public ModelAndView showBanks(HttpSession session){
        ModelAndView modelAndView = new ModelAndView("User");
        modelAndView.addObject("banks", userService.findAll());

        session.setAttribute("name", "User");
        historySessionService.addSession(session);
        return modelAndView;

    }

    @RequestMapping(path = "/userItem", method = RequestMethod.GET)
    public String loadUser (@RequestParam(value = "id",required = false) int id, Model model) {
        System.out.println("Dwadwa");
        model.addAttribute("id",id);
        return "redirect:home/" + id;
    }


    @RequestMapping(path = "/userItem/add", method = RequestMethod.GET)
    public String addingUser(@RequestParam(value = "id",required = false) int id, Model model, HttpSession session) {
        userService.addUserBank(id,(int) session.getAttribute("userId"));
        model.addAttribute("id",id);
        logger.info("added user");
        return "redirect:home/" + id;
    }

}