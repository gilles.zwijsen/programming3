package be.kdg.programming3.BankGilles.programming3.presentation.controller;

import be.kdg.programming3.BankGilles.programming3.service.BankService;
import be.kdg.programming3.BankGilles.programming3.service.HistorySessionService;
import be.kdg.programming3.BankGilles.programming3.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
public class UserItemController {

    private final UserService userService;
    private final HistorySessionService historySessionService;
    private final BankService bankService;

    public UserItemController(UserService bankService, HistorySessionService historySessionService, BankService bankService1) {
        this.userService = bankService;
        this.historySessionService = historySessionService;
        this.bankService = bankService1;
    }



    @GetMapping("/userItem")
    public ModelAndView showBanks(@RequestParam(value = "id",required = false) int id, HttpSession session){
        ModelAndView modelAndView = new ModelAndView("Banks");
        modelAndView.addObject("banks", userService.findBankByUser(id));
        modelAndView.addObject("bankslist",bankService.getBanks());
        session.setAttribute("name", "BankItem");
        session.setAttribute("userId",id);
        historySessionService.addSession(session);
        return modelAndView;

    }

    //when adding it refers back to the last page otherwise it crashes due to the page is linked to a certain account
    @RequestMapping(path = "/userItem/{id}", method = RequestMethod.GET)
    public String loadBankItem (@RequestParam(value = "id",required = false) int id, Model model, HttpSession session, HttpServletRequest request) {
        userService.addUserBank(id,(int) session.getAttribute("userId"));
        model.addAttribute("id",id);
        String referer = request.getHeader("Referer");
        return "redirect:"+ referer;
    }





}
