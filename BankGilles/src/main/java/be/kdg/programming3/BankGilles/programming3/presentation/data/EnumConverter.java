package be.kdg.programming3.BankGilles.programming3.presentation.data;

import be.kdg.programming3.BankGilles.programming3.Banks;
import org.springframework.core.convert.converter.Converter;
public class EnumConverter implements Converter<String,
        Banks> {
    @Override
    public Banks convert(String source) {
        if (source.toLowerCase().startsWith("ABN")) return Banks.ABNAMRO;
        if (source.toLowerCase().startsWith("AME")) return  Banks.AMERICANEXPRESS;
        if (source.toLowerCase().startsWith("RAB")) return  Banks.RABOBANK;
        if (source.toLowerCase().startsWith("KDC")) return  Banks.KDC;
        return Banks.ABNAMRO;//default value...
    }
}