package be.kdg.programming3.BankGilles.programming3.presentation.data;

import be.kdg.programming3.BankGilles.programming3.domain.Bank;
import be.kdg.programming3.BankGilles.programming3.domain.BankCard;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

public class JSonWriter {
    private static final String BANKS_JSON = "banks.json";
    private static final String BANKCARDS_JSON = "bankCards.json";
    private Gson gson;

    public JSonWriter() {
        GsonBuilder builder = new GsonBuilder();
        builder.setPrettyPrinting();
        builder.registerTypeAdapter(LocalDate.class, new LocalDateTimeSerializer());
        gson = builder.create();
    }

    public void writeBanks(List<Bank> banks){
        String json = gson.toJson(banks);
        try (FileWriter writer = new FileWriter(BANKS_JSON)) {
            writer.write(json);
        } catch (IOException e) {
            throw new RuntimeException("Unable to save bank objects to JSON", e);
        }
    }

    public void writeBankCards(List<BankCard> bankCards){
        String json = gson.toJson(bankCards);
        try (FileWriter writer = new FileWriter(BANKCARDS_JSON)) {
            writer.write(json);
        } catch (IOException e) {
            throw new RuntimeException("Unable to save bankcard-objects to JSON", e);
        }
    }
}
