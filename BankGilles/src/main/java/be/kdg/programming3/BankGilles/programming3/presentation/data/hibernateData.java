package be.kdg.programming3.BankGilles.programming3.presentation.data;


import be.kdg.programming3.BankGilles.programming3.Banks;
import be.kdg.programming3.BankGilles.programming3.domain.Bank;
import be.kdg.programming3.BankGilles.programming3.domain.BankCard;
import be.kdg.programming3.BankGilles.programming3.domain.User;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import java.time.LocalDate;

@Component
@Profile("hibernate")
public class hibernateData implements CommandLineRunner {

    @PersistenceUnit
    private EntityManagerFactory entityManagerFactory;


    @Override
    public void run(String... args) throws Exception {
        EntityManager entityManager =
                entityManagerFactory.createEntityManager();


        User user = new User("gilles", LocalDate.now().minusYears(14),"kapelWeidtje 8");
        User user2 = new User("Kasper",LocalDate.now().minusYears(16),"street");
        User user3 = new User("joe",LocalDate.now().minusYears(17),"klok" );
        User user4 = new User("malone",LocalDate.now().minusYears(55),"street" );


        Bank bank = new Bank(Banks.ABNAMRO,"Hopland","Belgium");
        Bank bank2 = new Bank(Banks.KDC,"jezusstraat","Belgium");
        Bank bank3 = new Bank(Banks.AMERICANEXPRESS,"laagland","Belgium");

        BankCard bankCard = new BankCard("lmao","BE1234242424242424",LocalDate.now().minusYears(1),421,1,1);
        BankCard bankCard2 = new BankCard("xd","BE1234242424242423",LocalDate.now().minusYears(1),125,2,1);
        BankCard bankCard3 = new BankCard("lmao","BE1234242424242422",LocalDate.now().minusYears(1),1111,3,1);



        user.addBank(bank);
        bank.AddUser(user);
        user.addBankCard(bankCard);
        bank.addBankCard(bankCard);

        user.addBank(bank2);
        bank2.AddUser(user);

        user2.addBank(bank);
        bank.AddUser(user2);
        user2.addBank(bank2);
        bank2.AddUser(user2);

        user3.addBank(bank);
        bank.AddUser(user3);

        user4.addBank(bank);
        bank.AddUser(user3);


        entityManager.getTransaction().begin();
        entityManager.persist(user);
        entityManager.persist(user2);
        entityManager.persist(user3);
        entityManager.persist(user4);
        entityManager.persist(bank2);

        entityManager.persist(bank3);

//        entityManager.persist(bankCard);
        entityManager.persist(bank);
        entityManager.getTransaction().commit();
        entityManager.close();


        System.out.println("swag");

    }
}
