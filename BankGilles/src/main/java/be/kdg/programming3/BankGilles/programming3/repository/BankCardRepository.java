package be.kdg.programming3.BankGilles.programming3.repository;

import be.kdg.programming3.BankGilles.programming3.domain.BankCard;

import java.util.List;


public interface BankCardRepository {


    BankCard createBankCard(BankCard bankcard);
    List<BankCard> readBankCards();
    BankCard getBankCardById(int id);
    List<BankCard> findByBank(int bankId, int userId);
}
