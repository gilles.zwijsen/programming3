package be.kdg.programming3.BankGilles.programming3.repository;

import be.kdg.programming3.BankGilles.programming3.domain.BankCard;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


@Repository
@Component
@Profile("Service")
public class BankCardRepositoryImplementation implements BankCardRepository {

    static final List<BankCard> bankCards = new ArrayList<>();

    @Override
    public BankCard createBankCard(BankCard bankCard) {
        bankCard.setBankId(bankCard.getBankId());
        bankCard.setId(bankCards.size()-1);
        bankCards.add(bankCard);
        return bankCard;
    }

    @Override
    public List<BankCard> readBankCards() {
        return bankCards;
    }

    @Override
    public BankCard getBankCardById(int id) {
      return bankCards.stream().filter(bankCard -> bankCard.getId() == id).findFirst().get();
    }

    @Override
    public List<BankCard> findByBank(int bankId, int userId) {
        return bankCards.stream().filter(bankCard -> bankCard.getBankId() == bankId).collect(Collectors.toList());
    }


}
