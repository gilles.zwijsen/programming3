package be.kdg.programming3.BankGilles.programming3.repository;

import be.kdg.programming3.BankGilles.programming3.domain.Bank;

import java.util.List;

public interface BankRepository {

    Bank createBank(Bank bank);
    List<Bank> readBanks();
    void deleteBank(int id, int userId);

}
