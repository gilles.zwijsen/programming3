package be.kdg.programming3.BankGilles.programming3.repository;

import be.kdg.programming3.BankGilles.programming3.domain.Bank;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Repository
@Component
@Profile("Service")
public class BankRepositoryImplementation implements BankRepository {

    static final List<Bank> banks = new ArrayList<>();



    @Override
    public Bank createBank(Bank bank) {
        bank.setBankId(banks.size()+1);
        banks.add(bank);
        return bank;
    }



    @Override
    public List<Bank> readBanks() {
      return banks;
    }


    @Override
    public void deleteBank(int id, int userId) {
        int bankid = banks.stream().filter(bank -> bank.getBankId() == id).collect(Collectors.toList()).get(0).getBankId();
        UserRepositoryImplementation.users.get(userId-1).getBanks().remove(UserRepositoryImplementation.users.get(userId - 1).getBanks().stream().filter(bank -> bank.getBankId() == bankid).findFirst().get());
    }

}
