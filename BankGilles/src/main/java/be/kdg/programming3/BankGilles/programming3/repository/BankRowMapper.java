package be.kdg.programming3.BankGilles.programming3.repository;

import be.kdg.programming3.BankGilles.programming3.domain.BankCard;
import org.springframework.jdbc.core.RowMapper;
import java.sql.ResultSet;
import java.sql.SQLException;

public class BankRowMapper implements RowMapper<BankCard> {


    @Override
    public BankCard mapRow(ResultSet rs, int rowNum) throws SQLException {
        BankCard card = new BankCard();
        card.setName(rs.getString("NAME"));
        card.setIbanNumber(rs.getString("IBANNUMBER"));
        card.setExpireDate(rs.getDate("EXPIREDATE").toLocalDate());
        card.setAmount(rs.getInt("AMOUNT"));
        card.setId(rs.getInt("BANKCARDID"));


        return card;
    }
}
