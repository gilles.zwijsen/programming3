package be.kdg.programming3.BankGilles.programming3.repository;

import be.kdg.programming3.BankGilles.programming3.domain.Bank;
import be.kdg.programming3.BankGilles.programming3.domain.BankCard;
import be.kdg.programming3.BankGilles.programming3.Banks;
import be.kdg.programming3.BankGilles.programming3.domain.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Component
@Profile("Service")
public class DataFactory implements CommandLineRunner {

     public static List<Bank> banks;
     public static List<BankCard> bankCards;
     public static List<User> users;
     public static int bankId = 1;
    private Logger logger = LoggerFactory.getLogger(DataFactory.class);


    private BankRepository bankObjectRepository;
    private BankCardRepository bankCardObjectRepository;
    private UserRepository userRepository;

    //add another repo
    public DataFactory(BankRepository bankObjectRepository, BankCardRepository bankCardRepository, UserRepository userRepository) {
        this.bankObjectRepository = bankObjectRepository;
        this.bankCardObjectRepository =bankCardRepository;
        this.userRepository = userRepository;
    }



//
//    Banks name;
//    String address;
//    String country;
//    public List<BankCard> cardList = new ArrayList<>();

    public static Bank makeRandomBank(){
        String[] arrayCountry = {"Netherlands","Greece","Spain","England","Africa","USA","France","Germany","Switzerland"};
        String[] arrayStreet = {"Longboy streey 12","shortboy street 1","Normal street 111","sad street 1","rembrand street"};

        Random random = new Random();







        return new Bank(Banks.values()[new Random().nextInt(Banks.values().length)],(arrayStreet[random.nextInt(arrayStreet.length)]),(arrayCountry[random.nextInt(arrayCountry.length)]),banks.size()+1);


    }


    public static BankCard makeRandomCard(){
        String[] array = {"jhon","jack","swag","gilles","tim","timmy","olaf","annie","marry"};

        Random random = new Random();

        LocalDate randomdate=  LocalDate.now().minus(random.nextInt(300)+100, ChronoUnit.MONTHS);


        return new BankCard((array[random.nextInt(array.length)]), ibanNumber(),
                randomdate,100*random.nextDouble(),1,1);


    }

    private static String ibanNumber() {
        Random rand = new Random();
        String card = "BE-";
        for (int i = 0; i < 16  ; i++)
        {
            int n = rand.nextInt(10) + 0;
            card += Integer.toString(n);


            if (i == 3 || i == 7 || i ==11 ) {
                card += "-";
            }

        }

        return card;

    }

    public void seed() {



         banks = new ArrayList<>();
         bankCards = new ArrayList<>();
         users = new ArrayList<>();



        userRepository.createUser("gilles",LocalDate.now(),"dwadwadwa");
        userRepository.createUser("Swaggy",LocalDate.now(),"dwadwadwa");
        userRepository.createUser("Kasper",LocalDate.now(),"dwadwadwa");

        Bank bank = makeRandomBank();
        Bank bank2 = makeRandomBank();
        Bank bank3 = makeRandomBank();
        Bank bank4 = makeRandomBank();
        Bank bank5 = makeRandomBank();
        Bank bank6 = makeRandomBank();

        BankCard bankCard = makeRandomCard();
        BankCard bankCard2 = makeRandomCard();
        BankCard bankCard3 = makeRandomCard();
        BankCard bankCard4 = makeRandomCard();
        BankCard bankCard5 = makeRandomCard();
        BankCard bankCard6 = makeRandomCard();


        userRepository.findById(0).getBanks().add(bank);
        userRepository.findById(0).getBanks().get(0).addBankCard(bankCard);

        userRepository.findById(0).getBanks().add(bank2);
        userRepository.findById(0).getBanks().get(0).addBankCard(bankCard2);

        userRepository.findById(0).getBanks().add(bank3);
        userRepository.findById(0).getBanks().get(1).addBankCard(bankCard3);

        userRepository.findById(1).getBanks().add(bank4);
        userRepository.findById(1).getBanks().get(0).addBankCard(bankCard4);


        userRepository.findById(2).getBanks().add(bank5);
        userRepository.findById(2).getBanks().get(0).addBankCard(bankCard5);

        userRepository.findById(2).getBanks().add(bank6);
        userRepository.findById(2).getBanks().get(0).addBankCard(bankCard6);

        banks.add(bank);
        banks.add(bank2);
        banks.add(bank3);
        banks.add(bank4);
        banks.add(bank5);
        banks.add(bank6);


        bankCards.add(bankCard);
        bankCards.add(bankCard2);
        bankCards.add(bankCard3);
        bankCards.add(bankCard4);
        bankCards.add(bankCard5);
        bankCards.add(bankCard6);

        banks.forEach(bankObjectRepository::createBank);
        bankCards.forEach(bankCardObjectRepository::createBankCard);





    }

    @Override
    public void run(String... args) throws Exception {
        seed();
    }


}
