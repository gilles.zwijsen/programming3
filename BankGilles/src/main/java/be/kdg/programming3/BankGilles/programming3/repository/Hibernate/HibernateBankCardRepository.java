package be.kdg.programming3.BankGilles.programming3.repository.Hibernate;

import be.kdg.programming3.BankGilles.programming3.domain.BankCard;
import be.kdg.programming3.BankGilles.programming3.repository.BankCardRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;
import java.util.List;

@Repository
@Profile("hibernate")
public class HibernateBankCardRepository implements BankCardRepository {
    private Logger logger = LoggerFactory.getLogger(HibernateBankCardRepository.class);


    @PersistenceUnit
    private javax.persistence.EntityManagerFactory entityManagerFactory;

    @Override
    public BankCard createBankCard(BankCard bankcard) {

        EntityManager entityManager =
                entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        entityManager.persist(bankcard);
        entityManager.getTransaction().commit();
        entityManager.close();
        return bankcard;
    }

    @Override
    public List<BankCard> readBankCards() {
        return null;
    }




    @Override
    public BankCard getBankCardById(int id) {

        EntityManager em = entityManagerFactory.createEntityManager();

        em.getTransaction().begin();
        BankCard bankCard = em.find(BankCard.class, id);
        em.getTransaction().commit();
        em.close();
        return bankCard;
    }

    @Override
    public List<BankCard> findByBank(int bankId, int userId) {
        EntityManager em = entityManagerFactory.createEntityManager();

        Query query = em.createQuery("SELECT p FROM BankCard p JOIN p.bank banks WHERE banks.bankId = :bankId and p.userId = :userId");
        query.setParameter("bankId", bankId);
        query.setParameter("userId", userId);
        return query.getResultList();
    }
}
