package be.kdg.programming3.BankGilles.programming3.repository.Hibernate;

import be.kdg.programming3.BankGilles.programming3.domain.Bank;
import be.kdg.programming3.BankGilles.programming3.repository.BankRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;
import java.util.List;

@Repository
@Profile("hibernate")
public class HibernateBankRepository  implements BankRepository {
    private Logger logger = LoggerFactory.getLogger(HibernateBankRepository.class);

    @PersistenceUnit
    private javax.persistence.EntityManagerFactory entityManagerFactory;



    @Override
    public Bank createBank(Bank bank) {

        EntityManager entityManager =
                entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        entityManager.persist(bank);
        entityManager.getTransaction().commit();
        entityManager.close();
        return bank;
    }

    @Override
    public List<Bank> readBanks() {
        EntityManager em = entityManagerFactory.createEntityManager();
        em.getTransaction().begin();
        List<Bank> banks = em.createQuery(" select b from Bank b ").getResultList();
        em.getTransaction().commit();
        em.close();
        return banks;
    }


    @Override
    public void deleteBank(int id, int userId) {
        EntityManager em = entityManagerFactory.createEntityManager();
        em.getTransaction().begin();

        Query query2 = em.createNativeQuery("delete from USERS_BANKS where BANK_ID = :id and USER_ID = :userId");
        query2.setParameter("id",id);
        query2.setParameter("userId",userId);

        query2.executeUpdate();

        em.getTransaction().commit();
        em.close();


    }

}
