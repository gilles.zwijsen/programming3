package be.kdg.programming3.BankGilles.programming3.repository.Hibernate;


import be.kdg.programming3.BankGilles.programming3.domain.Bank;
import be.kdg.programming3.BankGilles.programming3.domain.User;
import be.kdg.programming3.BankGilles.programming3.repository.UserRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

@Repository
@Profile("hibernate")
public class HibernateUserRepository implements UserRepository {

    @PersistenceUnit
    private javax.persistence.EntityManagerFactory entityManagerFactory;



    @Override
    public List<User> findAll() {
        EntityManager em = entityManagerFactory.createEntityManager();
        em.getTransaction().begin();
        List<User> users = em.createQuery(" select b from User b ").getResultList();
        em.getTransaction().commit();
        em.close();
        return users;
    }

    @Override
    public List<User> findByBank(int bankid) {
        return null;
    }

    @Override
    public User findById(int id) {
        EntityManager em = entityManagerFactory.createEntityManager();

        Query query = em.createQuery("SELECT p FROM User p WHERE p.id = :userId");
        query.setParameter("userId", id);
        return (User) query.getResultList();
    }

    @Override
    public void createUser(String name, LocalDate expireDate, String address) {
        User user = new User(name,expireDate,address);

        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        entityManager.persist(user);
        entityManager.getTransaction().commit();
        entityManager.close();

    }


    @Override
    public List<Bank> findBankByUser(int id) {
        EntityManager em = entityManagerFactory.createEntityManager();

        Query query = em.createQuery("SELECT p FROM Bank p JOIN p.users users WHERE users.id = :userId");
        query.setParameter("userId", id);
        return query.getResultList();
    }


    @Transactional
    @Override
    public void addUserBank(int id, int userId) {
        EntityManager em = entityManagerFactory.createEntityManager();

        em.getTransaction().begin();

        em.createNativeQuery("INSERT INTO users_banks (BANK_ID, USER_ID) VALUES (?,?)")
                .setParameter(1, id)
                .setParameter(2, userId).executeUpdate();
        em.getTransaction().commit();
        em.close();


    }



}
