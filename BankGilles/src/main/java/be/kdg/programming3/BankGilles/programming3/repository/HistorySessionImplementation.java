package be.kdg.programming3.BankGilles.programming3.repository;

import be.kdg.programming3.BankGilles.programming3.domain.HistorySession;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Repository
@Component
public class HistorySessionImplementation  implements HistorySessionRepository{

    static final  List<HistorySession> sessions = new ArrayList<>();


    @Override
    public HistorySession createSession(HttpSession session) {
        HistorySession sessionC = new HistorySession((String) session.getAttribute("name"));
         sessions.add(sessionC);
         return sessionC;
    }

    @Override
    public List<HistorySession> readSessions() {
        return sessions;
    }
}
