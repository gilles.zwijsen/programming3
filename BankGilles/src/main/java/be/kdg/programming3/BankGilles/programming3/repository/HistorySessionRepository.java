package be.kdg.programming3.BankGilles.programming3.repository;

import be.kdg.programming3.BankGilles.programming3.domain.HistorySession;

import javax.servlet.http.HttpSession;
import java.util.List;

public interface HistorySessionRepository {

    HistorySession createSession(HttpSession session);
    List<HistorySession> readSessions();
}
