package be.kdg.programming3.BankGilles.programming3.repository.JDBC;


import be.kdg.programming3.BankGilles.programming3.domain.BankCard;
import be.kdg.programming3.BankGilles.programming3.repository.BankCardRepository;
import be.kdg.programming3.BankGilles.programming3.repository.BankRowMapper;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Repository
@Profile("jdbc")
public class JDBCBankCardRepository implements BankCardRepository {
    private JdbcTemplate jdbcTemplate;
    private SimpleJdbcInsert personInserter;

    public JDBCBankCardRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.personInserter = new SimpleJdbcInsert(jdbcTemplate)
                .withTableName("BANKCARDS")
                .usingGeneratedKeyColumns("BANKCARDID");
    }


    public static BankCard mapBankCardsRow(ResultSet rs, int rowid) throws SQLException {
        return new BankCard(rs.getString("NAME"),
        rs.getString("IBANNUMBER")
        ,rs.getDate("EXPIREDATE").toLocalDate(),
        rs.getDouble("AMOUNT"),

        rs.getInt("BANKCARDID"),rs.getInt("BANKID"),rs.getInt("USERID"));
    }



    @Override
    public BankCard createBankCard(BankCard bankcard) {
        System.out.println(bankcard);
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("NAME", bankcard.getName());
        parameters.put("IBANNUMBER", bankcard.getIbanNumber());
        parameters.put("EXPIREDATE", bankcard.getExpireDate());
        parameters.put("AMOUNT", bankcard.getAmount());
        parameters.put("BANKID",bankcard.getBankId());
        parameters.put("USERID",bankcard.getUserId());
        bankcard.setId(personInserter.executeAndReturnKey(parameters).intValue());
        return bankcard;
    }

    @Override
    public List<BankCard> readBankCards() {
        String sql = "SELECT * FROM BANKCARDS";

        List<BankCard> cards = jdbcTemplate.query(
                sql,
                new BankRowMapper());

        return cards;
    }




    @Override
    public BankCard getBankCardById(int id) {
        String sql = "SELECT * FROM BANKCARDS WHERE BANKCARDID = ?";

        return jdbcTemplate.queryForObject(sql, new Object[]{id}, new BankRowMapper());
    }

    @Override
    public List<BankCard> findByBank(int bankId,int userId) {
        List<BankCard> bankCards = jdbcTemplate.query("SELECT * FROM BAnkcards WHERE BANKID = ? ANd USERID = ?", JDBCBankCardRepository::mapBankCardsRow, bankId,userId);
        return bankCards;

    }
}
