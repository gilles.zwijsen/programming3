package be.kdg.programming3.BankGilles.programming3.repository.JDBC;


import be.kdg.programming3.BankGilles.programming3.Banks;
import be.kdg.programming3.BankGilles.programming3.domain.Bank;
import be.kdg.programming3.BankGilles.programming3.repository.BankRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


@Repository
@Profile("jdbc")

public class JDBCBankRepository implements BankRepository {

    private Logger logger = LoggerFactory.getLogger(this.getClass());
    @Value("${spring.datasource.url}")
    private String dbURL;

    private JdbcTemplate jdbcTemplate;

    public JDBCBankRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;

    }



    @Override
    public Bank createBank(Bank bank) {
        Bank createdStudent = new Bank(bank.getName(), bank.getAddress(), bank.getCountry(),bank.getBankId());
        try (Connection connection = DriverManager.getConnection(dbURL)) {
            try (PreparedStatement statement
                         = connection.prepareStatement("INSERT INTO Banks(NAME, ADDRESS,BANKID, COUNTRY) VALUES ( ?,?,?,? )",
                    Statement.RETURN_GENERATED_KEYS)) {
                statement.setString(1, bank.getName().toString());
                statement.setString(2, bank.getAddress());
                statement.setInt(3, bank.getBankId());
                statement.setString(4, bank.getCountry());

                int result = statement.executeUpdate();
                if (result != 0) {
                    try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
                        if (generatedKeys.next()) {
                            createdStudent.setBankId(generatedKeys.getInt(1));
                        } else {
                            throw new SQLException("Creating bank failed, no ID obtained.");
                        }
                    }
                }
            }
        } catch (SQLException e) {
            logger.error("Problem creating the bank...", e);
        }
        return createdStudent;

    }


    @Override
    public List<Bank> readBanks() {
        List<Bank> students = new ArrayList<>();
        try (Connection connection =
                     DriverManager.getConnection(dbURL)) {
            try (Statement statement = connection.createStatement()) {
                try (ResultSet resultSet = statement.executeQuery("SELECT * FROM Banks")) {
                    while (resultSet.next()) {
                        int id = resultSet.getInt("BANKID");
                        String name = resultSet.getString("NAME");
                        String Address = resultSet.getString("ADDRESS");
                        String Country = resultSet.getString("COUNTRY");
                        Bank newStudent = new Bank(Banks.valueOf(name),Address,Country,id);
                        students.add(newStudent);
                    }
                }
            }
            return students;
        } catch (SQLException e) {
            logger.error("Something went wrong while retreiving all banks:", e);
            throw new RuntimeException(e);
        }

    }



    @Override
    public void deleteBank(int bankId, int userId) {
                jdbcTemplate.update("DELETE FROM BANKCARDS WHERE BANKID=? and USERID = ?", bankId,userId);
                jdbcTemplate.update("DELETE FROM USERS_BANKS WHERE BANK_ID=? and USER_ID=?", bankId,userId);
    }


}
