package be.kdg.programming3.BankGilles.programming3.repository.JDBC;

import be.kdg.programming3.BankGilles.programming3.Banks;
import be.kdg.programming3.BankGilles.programming3.domain.Bank;
import be.kdg.programming3.BankGilles.programming3.domain.User;
import be.kdg.programming3.BankGilles.programming3.repository.UserRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Repository
@Profile("jdbc")
public class JDBCUserRepository implements UserRepository {
    private JdbcTemplate jdbcTemplate;
    private SimpleJdbcInsert studentInserter;

    public JDBCUserRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.studentInserter = new SimpleJdbcInsert(jdbcTemplate)
                .withTableName("User")
                .usingGeneratedKeyColumns("ID");
    }

    public static User mapStudentRow(ResultSet rs, int rowid) throws SQLException {
        return new User(rs.getInt("ID"),
                rs.getString("NAME"),
                rs.getDate("DATEOFBIRTH").toLocalDate(),
                rs.getString("ADDRESS"));

    }

    public static Bank mapBankRow(ResultSet rs, int rowid) throws SQLException {
        int id = rs.getInt("BANKID");
        String name = rs.getString("NAME");
        String Address = rs.getString("ADDRESS");
        String Country = rs.getString("COUNTRY");
        return new Bank(Banks.valueOf(name),Address,Country,id);
    }




    @Override
    public List<User> findAll() {
        List<User> students = jdbcTemplate.query("SELECT * FROM Users", JDBCUserRepository::mapStudentRow);
        students.forEach(this::loadBanks);
        return students;

    }

    private void loadBanks(User user){
        List<Bank> courses = jdbcTemplate.query("SELECT * FROM banks WHERE bankID IN " +
                "(SELECT id FROM users WHERE id = ?) ", JDBCUserRepository::mapBankRow, user.getId());
        user.setBanks(courses);

    }



    @Override
    public List<User> findByBank(int bankid) {
        List<User> banks = jdbcTemplate.query("SELECT * FROM users WHERE ID IN " +
                "(SELECT User_ID FROM USERS_BANKS WHERE BANK_ID = ?)", JDBCUserRepository::mapStudentRow, bankid);
        return banks;
    }

    @Override
    public User findById(int id) {
        User user = jdbcTemplate.queryForObject("SELECT * FROM users WHERE ID = ?", JDBCUserRepository::mapStudentRow, id);
        if (user != null) {
            loadBanks(user);
        }
        return user;
    }

    @Override
    public void createUser(String name,LocalDate expireDate,String address) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("NAME", name);
        parameters.put("BIRTHDAY", Date.valueOf(expireDate));
        parameters.put("ADDRESS", address);
        if (address != null) {
            jdbcTemplate.update("INSERT INTO Users(NAME,DATEOFBIRTH,ADDRESS) VALUES ( ?, ?,?)",
                     name,expireDate, address);
        }
    }


    @Override
    public List<Bank> findBankByUser(int id) {
        return jdbcTemplate.query("SELECT * FROM banks WHERE bankID IN " +
                "(SELECT BANK_ID FROM USERS_BANKS WHERE USER_ID = ?) ", JDBCUserRepository::mapBankRow, id);
    }

    @Override
    public void addUserBank(int id, int userId) {
       jdbcTemplate.update("insert INTO USERS_BANKS (USER_ID, BANK_ID) VALUES (?, ?)",userId, id);
    }


}
