package be.kdg.programming3.BankGilles.programming3.repository.JPA;

import be.kdg.programming3.BankGilles.programming3.domain.Bank;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Profile("JPA")
@Repository()
public interface JPABankRepository  extends JpaRepository<Bank, Integer> {


    @Query("SELECT bank FROM Bank  bank WHERE bank.bankId = :id")
    Bank getBankById(@Param("id") int id);

    @Modifying
    @Transactional
    @Query(value = "delete from USERS_BANKS where BANK_ID = :bankId and USER_ID = :userId",nativeQuery = true)
    void deleteBank(@Param("bankId") int id, @Param("userId") int userId);
}
