package be.kdg.programming3.BankGilles.programming3.repository.JPA;

import be.kdg.programming3.BankGilles.programming3.domain.BankCard;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Profile("JPA")
@Repository
public interface JPABankcardRepository  extends JpaRepository<BankCard, Integer> {

    @Query("SELECT bankscards FROM BankCard  bankscards WHERE bankscards.bankId = :bankId ANd bankscards.userId = :userId")
    List<BankCard> findByBank(@Param("bankId") int bankId, @Param("userId") int userId);


    @Query("SELECT bankscards FROM BankCard  bankscards WHERE bankscards.id = :id")
    BankCard getBankCardById(@Param("id") int id);
}
