package be.kdg.programming3.BankGilles.programming3.repository.JPA;

import be.kdg.programming3.BankGilles.programming3.domain.HistorySession;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Profile("JPA")
@Repository
public interface JPAHistorySessionRepository extends JpaRepository<HistorySession, Integer> {
}
