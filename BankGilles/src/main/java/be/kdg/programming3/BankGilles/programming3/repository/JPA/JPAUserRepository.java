package be.kdg.programming3.BankGilles.programming3.repository.JPA;

import be.kdg.programming3.BankGilles.programming3.domain.Bank;
import be.kdg.programming3.BankGilles.programming3.domain.User;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Profile("JPA")
@Repository
public interface JPAUserRepository  extends JpaRepository<User, Integer> {

    @Query("SELECT p FROM Bank p JOIN p.users users WHERE users.id = :userId")
    List<Bank> findBankByUser(@Param("userId") int id);




    @Transactional
    @Modifying
    @Query(value =  "INSERT INTO users_banks (BANK_ID, USER_ID) VALUES (:id,:userId)",nativeQuery = true)
    void addUserBank(int id, int userId);
}
