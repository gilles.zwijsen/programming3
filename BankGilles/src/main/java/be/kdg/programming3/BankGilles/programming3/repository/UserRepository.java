package be.kdg.programming3.BankGilles.programming3.repository;

import be.kdg.programming3.BankGilles.programming3.domain.Bank;
import be.kdg.programming3.BankGilles.programming3.domain.User;

import java.time.LocalDate;
import java.util.List;

public interface UserRepository {
    List<User> findAll();
    List<User> findByBank(int bankid);
    User findById(int id);
    void createUser(String name, LocalDate expireDate, String address);
    List<Bank>  findBankByUser(int id);
    void addUserBank(int id, int userId);



}
