package be.kdg.programming3.BankGilles.programming3.repository;


import be.kdg.programming3.BankGilles.programming3.domain.Bank;
import be.kdg.programming3.BankGilles.programming3.domain.User;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;


@Repository
@Component
@Profile("Service")
public class UserRepositoryImplementation implements UserRepository {


    static final  List<User> users = new ArrayList<>();
        public static List<Bank> banks;


    public UserRepositoryImplementation() {
    }

    @Override
    public List<User> findAll() {
        return users;
    }

    @Override
    public List<User> findByBank(int bankid) {
        return null;
    }

    @Override
    public User findById(int id) {
        return users.get(id);
    }

    @Override
    public void createUser(String name, LocalDate expireDate, String address) {
        users.add(new User(name,expireDate,address));
        users.get(users.size()-1).setId(users.size());
    }

    @Override
    public List<Bank> findBankByUser(int id) {
        return users.get(id-1).getBanks();
    }

    @Override
    public void addUserBank(int id, int userId) {
        System.out.println(id);
        users.get(userId-1).addBank(BankRepositoryImplementation.banks.get(id-1));
    }


}
