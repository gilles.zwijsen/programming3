package be.kdg.programming3.BankGilles.programming3.service;

import be.kdg.programming3.BankGilles.programming3.domain.BankCard;

import java.time.LocalDate;
import java.util.List;

public interface BankCardService {

    BankCard addBankCard(String name, String ibanNumber, LocalDate expireDate, double amount,int bankId,int userId);
    List<BankCard> getBankCards();
    BankCard getBankCardById(int id);
    List<BankCard> findByBank(int bankId,int userId);
}
