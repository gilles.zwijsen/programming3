package be.kdg.programming3.BankGilles.programming3.service;

import be.kdg.programming3.BankGilles.programming3.domain.BankCard;
import be.kdg.programming3.BankGilles.programming3.repository.BankCardRepository;
import be.kdg.programming3.BankGilles.programming3.utils.BankCardLimit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Component
@Profile({"hibernate","jdbc","Service"})
public class BankCardServiceImplementation implements BankCardService{


    private BankCardRepository repository;

    @Autowired
    public BankCardServiceImplementation(BankCardRepository repository){
        this.repository = repository;
    }

    @Override
    public BankCard addBankCard(String name, String ibanNumber, LocalDate expireDate, double amount, int bankId, int userId) {
        BankCardLimit.checkLimit(repository.findByBank(bankId,userId).size());
        return repository.createBankCard(new BankCard(name,ibanNumber,expireDate,amount,0,bankId,userId));
    }

    @Override
    public List<BankCard> getBankCards() {
        return repository.readBankCards();
    }

    @Override
    public BankCard getBankCardById(int id) {
       return repository.getBankCardById(id);
    }

    @Override
    public List<BankCard> findByBank(int bankId,int userId) {
        return repository.findByBank(bankId,userId);
    }
}
