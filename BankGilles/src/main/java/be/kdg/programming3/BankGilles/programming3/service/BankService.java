package be.kdg.programming3.BankGilles.programming3.service;

import be.kdg.programming3.BankGilles.programming3.domain.Bank;
import be.kdg.programming3.BankGilles.programming3.Banks;

import java.util.List;

public interface BankService {

    Bank addBank(Banks name, String address, String country);


    List<Bank> getBanks();

    void deleteBank(int id, int userId);






}
