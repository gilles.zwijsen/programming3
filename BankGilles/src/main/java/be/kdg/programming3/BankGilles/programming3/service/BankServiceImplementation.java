package be.kdg.programming3.BankGilles.programming3.service;

import be.kdg.programming3.BankGilles.programming3.domain.Bank;
import be.kdg.programming3.BankGilles.programming3.Banks;
import be.kdg.programming3.BankGilles.programming3.repository.BankRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Profile({"hibernate","jdbc","Service"})
public class BankServiceImplementation implements BankService {

    private BankRepository repository;

    @Autowired
    public BankServiceImplementation(BankRepository repository){
        this.repository = repository;
    }

    @Override
    public Bank addBank(Banks name, String address, String country) {
        return repository.createBank(new Bank(name,address,country));

    }



    @Override
    public List<Bank> getBanks() {
        return repository.readBanks();
    }

    @Override
    public void deleteBank(int id,int userId) {
        repository.deleteBank(id,userId);
    }


}
