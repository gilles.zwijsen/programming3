package be.kdg.programming3.BankGilles.programming3.service;

import be.kdg.programming3.BankGilles.programming3.domain.HistorySession;

import javax.servlet.http.HttpSession;
import java.util.List;

public interface HistorySessionService {

    void addSession(HttpSession session);


    List<HistorySession> getSessions();
}
