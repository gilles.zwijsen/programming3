package be.kdg.programming3.BankGilles.programming3.service;

import be.kdg.programming3.BankGilles.programming3.domain.HistorySession;
import be.kdg.programming3.BankGilles.programming3.repository.HistorySessionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpSession;
import java.util.List;

@Component
@Profile({"hibernate","jdbc","Service"})
public class HistorySessionServiceImplementation implements HistorySessionService{


    private HistorySessionRepository repository;

    @Autowired
    public HistorySessionServiceImplementation(HistorySessionRepository repository){
        this.repository = repository;
    }

    @Override
    public void addSession(HttpSession session) {
        repository.createSession(session);
    }

    @Override
    public List<HistorySession> getSessions() {
        return repository.readSessions();
    }
}
