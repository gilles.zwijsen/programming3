package be.kdg.programming3.BankGilles.programming3.service.JPA;

import be.kdg.programming3.BankGilles.programming3.Banks;
import be.kdg.programming3.BankGilles.programming3.domain.Bank;
import be.kdg.programming3.BankGilles.programming3.repository.JPA.JPABankRepository;
import be.kdg.programming3.BankGilles.programming3.service.BankService;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.List;

@Profile("JPA")
@Service
public class JPABankServiceImpl implements BankService {

    JPABankRepository bankRepository;

    public JPABankServiceImpl(JPABankRepository bankRepository) {
        this.bankRepository = bankRepository;
    }

    @Override
    public Bank addBank(Banks name, String address, String country) {
        return bankRepository.save(new Bank(name,address,country));
    }

    @Override
    public List<Bank> getBanks() {
        return bankRepository.findAll();
    }

    @Override
    public void deleteBank(int id, int userId) {
        System.out.println("here");
       bankRepository.deleteBank(id,userId);
    }


}
