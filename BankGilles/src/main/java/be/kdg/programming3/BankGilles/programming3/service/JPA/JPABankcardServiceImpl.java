package be.kdg.programming3.BankGilles.programming3.service.JPA;

import be.kdg.programming3.BankGilles.programming3.domain.BankCard;
import be.kdg.programming3.BankGilles.programming3.repository.JPA.JPABankcardRepository;
import be.kdg.programming3.BankGilles.programming3.service.BankCardService;
import be.kdg.programming3.BankGilles.programming3.utils.BankCardLimit;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Profile("JPA")
@Service
public class JPABankcardServiceImpl implements BankCardService {
    private JPABankcardRepository bankcardRepository;

    public JPABankcardServiceImpl(JPABankcardRepository bankcardRepository) {
        this.bankcardRepository = bankcardRepository;
    }

    @Override
    public BankCard addBankCard(String name, String ibanNumber, LocalDate expireDate, double amount, int bankId, int userId) {
        BankCardLimit.checkLimit(bankcardRepository.findByBank(bankId,userId).size());
        return bankcardRepository.save(new BankCard(name,ibanNumber,expireDate,amount,bankId,userId));

    }

    @Override
    public List<BankCard> getBankCards() {
        return bankcardRepository.findAll();
    }

    @Override
    public BankCard getBankCardById(int id) {
        return bankcardRepository.getBankCardById( id);
    }

    @Override
    public List<BankCard> findByBank(int bankId, int userId) {
        return bankcardRepository.findByBank(bankId,userId);
    }
}
