package be.kdg.programming3.BankGilles.programming3.service.JPA;

import be.kdg.programming3.BankGilles.programming3.domain.HistorySession;
import be.kdg.programming3.BankGilles.programming3.repository.JPA.JPAHistorySessionRepository;
import be.kdg.programming3.BankGilles.programming3.service.HistorySessionService;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.util.List;

@Profile("JPA")
@Service

public class JPAHistoryServiceImpl implements HistorySessionService {

    JPAHistorySessionRepository jpaHistorySessionRepository;

    public JPAHistoryServiceImpl(JPAHistorySessionRepository jpaHistorySessionRepository) {
        this.jpaHistorySessionRepository = jpaHistorySessionRepository;
    }

    @Override
    public void addSession(HttpSession session) {
        jpaHistorySessionRepository.save(new HistorySession(session.getAttribute("name").toString()));

    }

    @Override
    public List<HistorySession> getSessions() {
        return jpaHistorySessionRepository.findAll();
    }
}
