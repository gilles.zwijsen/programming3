package be.kdg.programming3.BankGilles.programming3.service.JPA;

import be.kdg.programming3.BankGilles.programming3.domain.Bank;
import be.kdg.programming3.BankGilles.programming3.domain.User;
import be.kdg.programming3.BankGilles.programming3.repository.JPA.JPAUserRepository;
import be.kdg.programming3.BankGilles.programming3.service.UserService;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Profile("JPA")
@Service
public class JPAUserServiceImpl implements UserService {

    JPAUserRepository userRepository;

    public JPAUserServiceImpl(JPAUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public List<User> findByBank(int bankid) {
        return null;
    }



    @Override
    public void createUser(String name, LocalDate birthDate, String Address) {
        userRepository.save(new User(name,birthDate,Address));
    }





    @Override
    public List<Bank> findBankByUser(int id) {

       return userRepository.findBankByUser(id);
    }

    @Override
    public void deleteBank(int bankId, int userId) {

//        userRepository.deleteBank(bankId,userId);
    }

    @Override
    public void addUserBank(int id, int userId) {

        userRepository.addUserBank(id,userId);

    }
}
