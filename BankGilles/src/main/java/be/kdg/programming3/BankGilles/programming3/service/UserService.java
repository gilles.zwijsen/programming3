package be.kdg.programming3.BankGilles.programming3.service;

import be.kdg.programming3.BankGilles.programming3.domain.Bank;
import be.kdg.programming3.BankGilles.programming3.domain.User;

import java.time.LocalDate;
import java.util.List;

public interface UserService {

    List<User> findAll();
    List<User> findByBank(int bankid);
    void createUser(String name, LocalDate birthDate, String Address);
    List<Bank>  findBankByUser(int id);
    void deleteBank(int bankId, int userId);
    void addUserBank(int id, int userId);
}
