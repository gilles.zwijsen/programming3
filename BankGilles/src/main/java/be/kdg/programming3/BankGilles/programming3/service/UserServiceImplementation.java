package be.kdg.programming3.BankGilles.programming3.service;

import be.kdg.programming3.BankGilles.programming3.domain.Bank;
import be.kdg.programming3.BankGilles.programming3.domain.User;
import be.kdg.programming3.BankGilles.programming3.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.List;

@Component
@Profile({"hibernate","jdbc","Service"})
public class UserServiceImplementation implements UserService {


    private UserRepository repository;
    private BankService bankService;

    @Autowired
    public UserServiceImplementation(UserRepository repository, BankService bankService){
        this.repository = repository;
        this.bankService= bankService;
    }

    @Override
    public List<User> findAll() {
        return repository.findAll();
    }

    @Override
    public List<User> findByBank(int bankid) {
        return repository.findByBank(bankid);
    }


    @Override
    public void createUser(String name, LocalDate birthDate, String Address) {
        repository.createUser(name,birthDate,Address);
    }

    @Override
    public List<Bank>  findBankByUser(int id) {
        return repository.findBankByUser(id);
    }

    @Override
    public void deleteBank(int bankId, int userId) {
        bankService.deleteBank(bankId,userId);
    }

    @Override
    public void addUserBank(int id, int userId) {
        repository.addUserBank(id,userId);
    }
}
