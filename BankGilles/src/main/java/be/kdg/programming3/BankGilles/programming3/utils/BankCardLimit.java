package be.kdg.programming3.BankGilles.programming3.utils;

//cant have more than 5 bankcards

import be.kdg.programming3.BankGilles.programming3.exceptions.BankCardLimitExecption;

public class BankCardLimit {

    static final int MAX_ACCOUNTS = 3;

    public static void checkLimit(int size) {
        if (size >= MAX_ACCOUNTS) throw new BankCardLimitExecption("You've reached the limit of bank accounts" );
    }

}
