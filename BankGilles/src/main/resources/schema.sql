
drop table if exists USERS_BANKS;
drop table if exists BankCards;
drop table if exists Banks;
drop table if exists Users;


create table if not exists Users (
     ID INTEGER auto_increment
     primary key
     unique,
     NAME varchar(50) not null,
     DATEOFBIRTH Date not null,
     ADDRESS varchar(50) not null


);




create table if not exists Banks
(
    NAME varchar(30) not null,
    ADDRESS varchar(30) not null ,
    COUNTRY varchar(30) not null,
    BANKID INTEGER auto_increment primary key unique

    );




create table if not exists BankCards
(
    NAME varchar(30) not null ,
    IBANNUMBER varchar(50) not null ,
    EXPIREDATE DATE not null ,
    AMOUNT INTEGER not null ,
    BANKCARDID INTEGER auto_increment primary key unique,
    BANKID INTEGER not null,
    constraint FK_BankCards_bankID
        foreign key (BANKID) references Banks,
    USERID INTEGER not null,
    constraint FK_BankCards_USERID
        foreign key (USERID) references Users



);

create table USERS_BANKS
(
    USER_ID INTEGER not null,
    BANK_ID  INTEGER not null,
    constraint FK_USER_BANKS_BANK_ID
        foreign key (BANK_ID) references Banks,
    constraint FK_USER_BANKS_USER_ID
        foreign key (USER_ID) references Users
);








