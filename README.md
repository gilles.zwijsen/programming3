# programming3

## Introduction

Hi, This project is made by Gilles Zwijsen and is a simple bank project.
There are 3 entities
- bank
- user
- bankCard

person and bank - Many to Many
bank and bankcard - One to Many 
person and bankCard - One to Many 

So with this application you can make multiple users and banks.
Than a user can add a certain bank to his account and create bankcards at the bank.


## Database

create a H2 database called "banksdb" with no auth
and a prostgress database called "hibernate" with no auth


## What can you do?

You can create your user.

You can create certain banks with different locations.

You can add a particular bank branch to a user.

You can add a Bankcard to that bank.

You can delete the relationship from a user with a bank and its bankcards.

You can save all bankcards as a json.

You can save bank filtered by user as json.


## Execptions

There is a specific execption for limited bankcards, there is a maximum of 3 allowed for a certain bank branch.


## Collection

The first repository is based on Collection, to use it head over to the application.properties file in te recourses folder.
Unqoute the first 3 lines so the profle "#spring.profiles.active=Service" is active.

## JDBC

The second repository is based on JDBC with a database, to use it head over to the application.properties file in te recourses folder.
the next block of code so that the profle "spring.profiles.active=jdbc" is active.

By using a h2Database

## Hibernate


The third repository is based on Hibernate, to use it head over to the application.properties file in te recourses folder.
the next block of code so that the profle "#spring.profiles.active=hibernate" is active.

By using a hibernate with a postgresql database

## JPA

The last repository is based on JPA, to use it head over to the application.properties file in te recourses folder.
the next block of code so that the profle "#spring.profiles.active=JPA" is active. 
than querry the code manually from the import.sql file to the MySQL(hibernate) database;

By using a hibernate with a postgresql database


# Thank you hans for your time to check out the project :)




